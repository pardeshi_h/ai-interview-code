
import numpy as np
import pandas as pd
import datetime as dt
import sys
from sklearn.externals import joblib

def load_model(filename):
	loaded_model = joblib.load(filename)
	return loaded_model

def read_inputs(filename):
	data = pd.read_csv(filename)
	input_data = format_data(data)
	return input_data

def format_data(data):
	feature_list = ('goal', 'pledged', 'backers', 'usd_pledged_real', 'usd_goal_real',
       'date_diff', 'main_category_Art', 'main_category_Comics',
       'main_category_Crafts', 'main_category_Dance', 'main_category_Design',
       'main_category_Fashion', 'main_category_Film & Video',
       'main_category_Food', 'main_category_Games', 'main_category_Journalism',
       'main_category_Music', 'main_category_Photography',
       'main_category_Publishing', 'main_category_Technology',
       'main_category_Theater', 'currency_AUD', 'currency_CAD', 'currency_CHF',
       'currency_DKK', 'currency_EUR', 'currency_GBP', 'currency_HKD',
       'currency_JPY', 'currency_MXN', 'currency_NOK', 'currency_NZD',
       'currency_SEK', 'currency_SGD', 'currency_USD', 'country_AT',
       'country_AU', 'country_BE', 'country_CA', 'country_CH', 'country_DE',
       'country_DK', 'country_ES', 'country_FR', 'country_GB', 'country_HK',
       'country_IE', 'country_IT', 'country_JP', 'country_LU', 'country_MX',
       'country_N,0"', 'country_NL', 'country_NO', 'country_NZ', 'country_SE',
       'country_SG', 'country_US')
	
	input_data  = pd.DataFrame(0, index=np.arange(len(data)), columns=feature_list)

	data['deadline'] = pd.to_datetime(data['deadline'])
	data['launched'] = pd.to_datetime(data['launched'])
	data['date_diff'] = data['deadline'].sub(data['launched'], axis = 0) 
	data['date_diff'] = data['date_diff'].astype(dt.timedelta).map(lambda x: np.nan if pd.isnull(x) else x.days)


	input_data['goal'] = data['goal']
	input_data['pledged'] = data['pledged']
	input_data['backers'] = data['backers']
	input_data['usd_pledged_real'] = data['usd_pledged_real']
	input_data['usd_goal_real'] = data['usd_goal_real']
	input_data['date_diff'] = data['date_diff']
	
	for i, row in data.iterrows():
		try:
			col_name_main_cat = 'main_category_'+row['main_category']
			input_data[col_name_main_cat][i] = 1
			col_name_curency = 'currency_'+row['currency']
			input_data[col_name_curency][i] = 1
			col_name_country = 'country_'+row['country']
			input_data[col_name_country][i] = 1
		except:
			print('Not a valid value for column Main Category or Currenncy or Country' )

	return input_data
	


if __name__ == "__main__":
	
	
	model_file = sys.argv[1]
	data_file = str(sys.argv[2])
	
	print(model_file)
	print(data_file)
	
	loaded_model = load_model(model_file)
	input_data = read_inputs(data_file)
    
	result = loaded_model.predict(input_data)
	print(result)